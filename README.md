# Cloudtoken

Cloudtoken is a command line utility for Unix environments for facilitating the steps required to authenticate with a
public cloud provider such as Amazon Web Services and retrieve access credentials that can then be used by applications
running in your local environment.

## Why is this needed

Acme company has decided to institute best practices for access to AWS to ensure the security of their infrastructure.
In order to do this they have decided that all employees must now assume a Federated IAM Role when accessing AWS and
authentication must be provided by their in-house ADFS server which will use SAML to federate the authentication to AWS.

Through the use of different plugins Cloudtoken can facilitate the different steps required the accomplish the above
goals.

Cloudtoken can:

* Authenticate with ADFS.
* Handle the SAML exchange with AWS.
* Assume a Federated IAM Role and obtain ephemeral access keys.
* Make the obtained access keys available to applications running in the local environment.

This is just a brief description of Cloudtoken, please see the full [README](https://bitbucket.org/atlassian/cloudtoken/src/4a6cbee173d98bb16408e75a330d02a5c98f170e/cloudtoken/README.md?at=master&fileviewer=file-view-default).

Each plugin in has its own README file also. Please browse the plugin directories to read them.

## Contributors

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes.
* Follow the existing style.
* Separate unrelated changes into multiple pull requests.

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate link below to digitally sign the CLA. The Corporate CLA is for those who are contributing as a member of an organization and the individual CLA is for those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## License

Copyright (c) 2016 Atlassian and others. Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassian/cloudtoken/src/4a6cbee173d98bb16408e75a330d02a5c98f170e/LICENSE.txt) file.