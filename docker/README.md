# Cloudtoken Docker

In order to operate docker containers on your local development environment with access to the cloudtoken proxy this 
docker-compose configuration will assist you setting things up. As part of the general package release we push a 
pre-built docker image to dockerhub: https://hub.docker.com/r/atlassiancloudteam/cloudtoken Using this image we are able
to add a docker network to your docker service and run the cloudtoken proxy inside a container connected to the network
on the standard 169.254.169.254 address.

## Setup process

  * Clone this repo onto your local system: `git clone git@bitbucket.org:atlassian/cloudtoken.git`
  * Move into the docker folder `cd cloudtoken/docker`
  * Copy your current config.yaml `cp ${HOME}/.config/cloudtoken/config.yaml ${HOME}/.config/cloudtoken/docker-config.yaml`
  * Edit your new `${HOME}/.config/cloudtoken/docker-config.yaml` and ensure the following:
    * Under defaults you have specified `username: <your idp username>`
    * Under plugins/post_auth you have only `export_credentials_json` listed.
  * Execute docker-compose run: `docker-compose run cloudtoken`
 
## Connecting other docker containers to the cloudtoken proxy

In order to use the cloudtoken container add the following to any other docker compose configurations:

```
# individual container config
container1:
...
networks:
  - cloudtoken

# top level network config
networks:
cloudtoken:
  external:
    name: docker_cloudtoken
# 
```
